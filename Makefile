CFLAGS+= -Wall
LDADD+= -lX11 
LDFLAGS=
EXEC=zwm

PREFIX?= /usr
BINDIR?= $(PREFIX)/bin

CC=gcc

all: $(EXEC)

zwm: zwm.o
	$(CC) $(LDFLAGS) -Os -o $@ $+ $(LDADD)

install: all
	install -Dm 755 zwm $(DESTDIR)$(BINDIR)/zwm

clean:
	rm -f zwm *.o
