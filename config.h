#ifndef CONFIG_H
#define CONFIG_H
#include <X11/X.h>
#include <stddef.h>
// Mod (Mod1 == alt / Mod4 == Super) and master size
#define MOD             Mod4Mask
#define MASTER_SIZE     0.6
#define barh     50
#define barbw    5

// Colors
#define FOCUS           "rgb:bc/57/66"
#define UNFOCUS         "rgb:88/88/88"

static const char* colorScheme[] = { "#665522", "#002030", "#003040", "#001020", "#00FF00",};
static const char *defaulttextcolor[] = { "#002030", "#002030", "#665544", "#998866", "#887733", "#999999", "#558844", "#aa6622", "#ffffff", "#003040", };
static const char *defaultwincolor[] = { "#443311", "#002030", };

const char* term[] = {"st",NULL};
const char* run_menu[] = {"dmenu_run",NULL};

// Shortcuts
static struct key keys[] = {
    // MOD              KEY                         FUNCTION        ARGS
    {  MOD,             XK_q,                       kill_client,    {NULL}},
    {  MOD,             XK_k,                       prev_win,        {NULL}},
    {  MOD,             XK_j,                       next_win,      {NULL}},
    {  MOD,             XK_Return,                  spawn,          {.com = term}},
    {  MOD,             XK_r,                       spawn,          {.com = run_menu}}
};

#endif
