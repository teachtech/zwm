#include <string.h>
#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/keysym.h>
#include <X11/XF86keysym.h>
#include <X11/Xatom.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <stddef.h>
#include <X11/Xutil.h>

#define TABLENGTH(X)    (sizeof(X)/sizeof(*X))

typedef union {
    const char** com;
    const int i;
} Arg;

// Structs
struct key {
    unsigned int mod;
    KeySym keysym;
    void (*function)(const Arg arg);
    const Arg arg;
};

typedef struct client client;
struct client{
    client *next;
    client *prev;
    Window win;
};

// Visible Functions for config
void kill_client();
void spawn(const Arg arg);
void prev_win();
void next_win();

// Include configuration file (need struct key)
#include "config.h"

// Invisible Functions
void grabkeys();
void setup();
void run();
void keypress(XEvent *e);
void maprequest(XEvent *e);
void destroynotify(XEvent *e);
void configurerequest(XEvent *e);
void propertynotify(XEvent *e);
void manage();
void add_window(Window w);
void remove_window(Window w);
static void update_current();
unsigned long getcolor(const char* color);
void set_bar();
void update_bar();

// Variable
static Display *dis;
static int master_size;
static int mode;
static int sh;
static int sw;
static int screen;
static Window root;
static client *head;
static Window bar;
static client *current;
static unsigned int win_focus, win_unfocus;

// Events array
static void (*events[LASTEvent])(XEvent *e) = {
    [KeyPress] = keypress,
    [MapRequest] = maprequest,
    [DestroyNotify] = destroynotify,
    [ConfigureRequest] = configurerequest,
    [PropertyNotify] = propertynotify
};

// Main program
int main(int argc, char **argv) {
    if(!(dis = XOpenDisplay(NULL)))
        return 1;
    setup();
    run();
    XCloseDisplay(dis);
    return 0;
}

// Implementations
void grabkeys() {
    int i;
    KeyCode code;
    for(i=0;i<TABLENGTH(keys);++i) {
        if((code = XKeysymToKeycode(dis,keys[i].keysym))) {
            XGrabKey(dis,code,keys[i].mod,root,True,GrabModeAsync,GrabModeAsync);
        }
    }
}


void setup() {
    screen = DefaultScreen(dis);
    root = RootWindow(dis,screen);
    sw = XDisplayWidth(dis,screen);
    sh = XDisplayHeight(dis,screen) - barh - barbw - 2;
    grabkeys();
    mode = 0;
    head = NULL;
    current = NULL;
    master_size = sw*MASTER_SIZE;

    set_bar();
    
    XSelectInput(dis,root,SubstructureNotifyMask|SubstructureRedirectMask);
}

void run() {
    XEvent ev;
    while(!XNextEvent(dis,&ev)) {
        //update_bar();
        if(events[ev.type]) {
            events[ev.type](&ev);
        }
        //update_bar();
    }
}

void manage() {
    client *c;
    int n = 0;
    int y = 0;
    if(head != NULL && head->next == NULL) {
        XMoveResizeWindow(dis,head->win,0,barh,sw-2,sh-2);
    }
    else if(head != NULL) {
        switch(mode) {
            case 0:
                XMoveResizeWindow(dis,head->win,0,barh,master_size-2,sh-2);
                for(c=head->next;c;c=c->next) ++n;
                for(c=head->next;c;c=c->next) {
                    XMoveResizeWindow(dis,c->win,master_size,barh+y,sw-master_size-2,(sh/n)-2);
                    y += sh/n;
                }
                break;
            case 1:
                for(c=head;c;c=c->next) {
                    XMoveResizeWindow(dis,c->win,0,barh,sw,sh);
                }
                break;
            default:
                break;
        }
    }
}

void update_current() {
    client *c;
    for(c=head;c;c=c->next)
        if(current == c) {
            XSetWindowBorderWidth(dis,c->win,1);
            XSetWindowBorder(dis,c->win,getcolor(colorScheme[4]));
            XSetInputFocus(dis,c->win,RevertToParent,CurrentTime);
            XRaiseWindow(dis,c->win);
        }
        else
            XSetWindowBorder(dis,c->win,getcolor(colorScheme[1]));
}

void spawn(const Arg arg) {
    if(fork() == 0) {
        if(fork() == 0) {
            if(dis)
                close(ConnectionNumber(dis));
            setsid();
            execvp((char*)arg.com[0],(char**)arg.com);
        }
        exit(0);
    }
}

void add_window(Window w) {
    client *c,*t;
    if(!(c = (client *)calloc(1,sizeof(client))))
        fprintf(stderr, "Could not allocate for a window");
    if(head == NULL) {
        c->next = NULL;
        c->prev = NULL;
        c->win = w;
        head = c;
    }
    else {
        for(t=head;t->next;t=t->next);
        c->next = NULL;
        c->prev = t;
        c->win = w;
        t->next = c;
    }
    current = c;
}

void remove_window(Window w) {
    client *c;
    for(c=head;c;c=c->next) {
        if(c->win == w) {
            if(c->prev == NULL && c->next == NULL) {
                free(head);
                head = NULL;
                current = NULL;
                return;
            }
            if(c->prev == NULL) {
                head = c->next;
                c->next->prev = NULL;
                current = c->next;
            }
            else if(c->next == NULL) {
                c->prev->next = NULL;
                current = c->prev;
            }
            else {
                c->prev->next = c->next;
                c->next->prev = c->prev;
                current = c->prev;
            }
            free(c);
            return;
        }
    }
}

// TODO: create a general function to send signals
void kill_client() {
    if(current != NULL) {
		XEvent ke;
		ke.type = ClientMessage;
		ke.xclient.window = current->win;
		ke.xclient.message_type = XInternAtom(dis, "WM_PROTOCOLS", True);
		ke.xclient.format = 32;
		ke.xclient.data.l[0] = XInternAtom(dis, "WM_DELETE_WINDOW", True);
		ke.xclient.data.l[1] = CurrentTime;
		XSendEvent(dis, current->win, False, NoEventMask, &ke);
    }
}

void next_win() {
    client *c;
    if(current != NULL && head != NULL) {
		if(current->next == NULL)
            c = head;
        else
            c = current->next;
        current = c;
        update_current();
    }
}

void prev_win() {
    client *c;
    if(current != NULL && head != NULL) {
        if(current->prev == NULL)
            for(c=head;c->next;c=c->next);
        else
            c = current->prev;
        current = c;
        update_current();
    }
}

/* NOT USED NOW => STILL NEEDS WORK*/
void focus_client() {
    if (current != NULL) {
		XEvent ke;
		ke.type = ClientMessage;
		ke.xclient.window = current->win;
		ke.xclient.message_type = XInternAtom(dis, "WM_PROTOCOLS", True);
		ke.xclient.format = 32;
		ke.xclient.data.l[0] = XInternAtom(dis, "WM_DELETE_WINDOW", True);
		ke.xclient.data.l[1] = CurrentTime;
		XSendEvent(dis, current->win, False, NoEventMask, &ke);
    }
}

void keypress(XEvent *e) {
    int i;
    XKeyEvent ke = e->xkey;
    KeySym keysym = XKeycodeToKeysym(dis,ke.keycode,0);
    for(i=0;i<TABLENGTH(keys);++i) {
        if(keys[i].keysym == keysym && keys[i].mod == ke.state) {
            keys[i].function(keys[i].arg);
        }
    }
}

void maprequest(XEvent *e) {
    XMapRequestEvent *ev = &e->xmaprequest;
    client *c;
    for(c=head;c;c=c->next)
        if(ev->window == c->win) {
            return;
        }
    XMapWindow(dis,ev->window);
    add_window(ev->window);
    manage();
    update_current();
}

void configurerequest(XEvent *e) {
    // Paste from DWM, thx again \o/
    XConfigureRequestEvent *ev = &e->xconfigurerequest;
    XWindowChanges wc;
    wc.x = ev->x;
    wc.y = ev->y;
    wc.width = ev->width;
    wc.height = ev->height;
    wc.border_width = ev->border_width;
    wc.sibling = ev->above;
    wc.stack_mode = ev->detail;
    XConfigureWindow(dis, ev->window, ev->value_mask, &wc);
}

void destroynotify(XEvent *e) {
    int i=0;
    client *c;
    XDestroyWindowEvent *ev = &e->xdestroywindow;
    for(c=head;c;c=c->next)
        if(ev->window == c->win)
            i++;
    if(i == 0)
        return;
    remove_window(ev->window);
    manage();
    update_current();
}

void propertynotify(XEvent *e)
{
    update_bar();
	XPropertyEvent *ev = &e->xproperty;

	if (ev->window == root) //&& (ev->atom == XA_WM_NAME))
		update_bar();
	else if (ev->state == PropertyDelete)
		return; /* ignore */
}

unsigned long getcolor(const char* color) {
    XColor c;
    Colormap map = DefaultColormap(dis,screen);

    if(XAllocNamedColor(dis,map,color,&c,&c)) return c.pixel;
    else {
        return 1;
    }
    
}

void set_bar() {    
    XSetWindowAttributes wa = {.background_pixmap = ParentRelative, .override_redirect = True, .event_mask = ButtonPressMask|ExposureMask};
    XClassHint ch = {"zwm", "zwm"};
    bar = XCreateWindow(dis, root, 0, 0, sw, barh, barbw, 
                                DefaultDepth(dis, screen),
                                CopyFromParent,
                                DefaultVisual(dis, screen),
                                CWOverrideRedirect|CWBackPixmap|CWEventMask,
                                &wa);
    XSetClassHint(dis, bar, &ch);
    XMapRaised(dis, bar);
    Atom wmatom;
    wmatom = XInternAtom(dis, "WM_NAME", False);
    XChangeProperty(dis, root, wmatom, XA_STRING, 8, PropModeReplace, (unsigned char*) "zwm-default", 10);
}

static char *estrndup(const char *s, size_t maxlen)
{
	char *p;

	if ((p = strndup(s, maxlen)) == NULL)
		fprintf(stderr, "strndup");
	return p;
}
void update_bar() {
    Atom wmatom;
    wmatom = XInternAtom(dis, "WM_NAME", False);

    XTextProperty tprop;
	char **list = NULL;
	char *name = NULL;
	unsigned char *p = NULL;
	unsigned long size, dl;
	int di;
	Atom da;

	if (XGetWindowProperty(dis, root, wmatom, 0L, 8L, False, XA_STRING, &da, &di, &size, &dl, &p) == Success && p) {
		name = estrndup((char *)p, 128);
		XFree(p);
	} else if (XGetWMName(dis, root, &tprop) && XmbTextPropertyToTextList(dis, &tprop, &list, &di) == Success && di > 0 && list && *list) {
		name = estrndup(*list, 128);
		XFreeStringList(list);
		XFree(tprop.value);
	} else {
        XChangeProperty(dis, root, wmatom, XA_STRING, 8, PropModeReplace, (unsigned char*) "not working", 10);
	}
    XChangeProperty(dis, root, wmatom, XA_STRING, 8, PropModeReplace, (unsigned char*) name, strlen(name));
    
    XSetWindowBackground(dis, bar, getcolor(colorScheme[3]));
    XSetForeground(dis, DefaultGC(dis, screen), getcolor(colorScheme[4]));
    XClearWindow(dis, bar);
    name ? XDrawString(dis, bar, DefaultGC(dis, screen), 10, 10, name, strlen(name)) : XDrawString(dis, bar, DefaultGC(dis, screen), barh, barh/2+2, "zwm-defaults", 12) ;

    fflush(stdout);
}
